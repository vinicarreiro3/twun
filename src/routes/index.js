const currencies = require('../controllers/currencies.js')
const routes = (app) => {
    app.post('/currencies', currencies);
}

module.exports = routes