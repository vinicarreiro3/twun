const request  = require('supertest');
const app = require('../app')
console.log(app)

describe('Currencies API', () => {
    it('POST /currencies ---> compare currencies', () => {
        return request(app).post('/currencies').send({
            "base_currency": "USD",
            "quote_currency": ["EUR","GBP"]
    }).expect('Content-Type', /json/).expect(201).then((response) => {
        expect(response.body[0]).toEqual(expect.objectContaining({
                base_currency: expect.any(String),
                quote_currency: expect.any(String),
                bid: expect.any(String),
                ask: expect.any(String),
                midpoint: expect.any(String)
        }))
    })

    })   
})
