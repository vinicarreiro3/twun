const express = require('express');
require("dotenv").config()
const routes = require("./routes");

const port = 3001;
let app = express();
app.use(express.json());

routes(app);

app.listen(port, () => {
});

module.exports = app
