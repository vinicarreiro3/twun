const axios = require('axios')
const r = require("ramda");

const currencies = async (req, res) =>{
    const { base_currency,quote_currency } = req.body;

    let quotes = ''
    let bases = ''

    const putBase = x =>  bases += `&base=${x}`;
    const putQuote = x =>  quotes += `&quote=${x}`;
    r.map(putBase, base_currency)
    r.map(putQuote, quote_currency)

    const oanda = () => axios.get(`${process.env.OANDA_URL}${bases}${quotes}`)

    const path = r.path(['data','quotes'])

    const pipeAsync = r.pipeWith(r.andThen)

    const currencies = pipeAsync([
        oanda,
        path
    ])

    return res.status(201).json(await currencies());
}

module.exports = currencies